import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { fieldActions, canvasActions } from '../_actions';
const { uuid } = require('uuidv4');
const fabric = window.fabric

class DesignCanvas extends React.Component {
  constructor(props) {
    super(props);
    this.getMousePos = this.getMousePos.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleTranslate = this.handleTranslate.bind(this);
  }

  static defaultProps = {
    width: 600,
    height: 400,
  }

  state = {
    canvas: null,
    fileLoaded: false
  }

  componentDidMount() {
    let { handleOnClickCanvas } = this.props;

    const canvas = new fabric.Canvas(this.c, {
      hoverCursor: 'pointer',
      selection: true,
      selectionBorderColor: 'green',
      backgroundColor: null,
      uniScaleTransform: true
    })
    canvas.setZoom(1)
    this.setState({ canvas });

    canvas.on("mouse:down", this.handleClick);
    canvas.on("object:modified", function () {
      var canvasObjs = {};
      this.getObjects().map(obj => {
        canvasObjs[obj.id] = { x: obj.left, y: obj.top, width: (obj.width * obj.scaleX), height: (obj.height * obj.scaleY) };
      });
      handleOnClickCanvas(canvasObjs);
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const { canvas } = this.state;
    if (this.props.fields.length < prevProps.fields.length) {
      let remove = prevProps.fields.filter(field => this.props.fields.indexOf(field) === -1).map(field => { return field.id });
      canvas.getObjects().filter(obj => remove.indexOf(obj.id) >= 0).forEach(obj => {
        canvas.remove(obj);
      })
    }
  }

  handleClick(e) {
    e.e.preventDefault();
    let canvas = this.state.canvas;
    let isCreate = canvas.getActiveObject() === null || canvas.getActiveObject() === undefined;
    if (isCreate) {
      var mousePos = this.getMousePos(e.e);
      var id = uuid();
      var rect = new fabric.Rect({ top: mousePos.y, left: mousePos.x, width: 40, height: 40, hasBorder: false, fill: '#ffdb93', opacity: 0.5, hasRotatingPoint: false, selectable: true, stroke: "#000", strokeWidth: 0, centeredRotation: true, borderColor: 'red', cornerColor: 'green', cornerSize: 8, transparentCorners: false, id: id });
      canvas.add(rect);
      this.props.addField({ id: id, isScaned: false, result: null, lang: 'eng', position: { x: mousePos.x, y: mousePos.y, width: 40, height: 40 } })
    }
  }

  getMousePos(evt) {
    let canvas = this.c;
    if (!canvas || !evt) return;
    var rect = canvas.getBoundingClientRect();
    return {
      x: evt.clientX - rect.left,
      y: evt.clientY - rect.top
    };
  }

  onChangeHandler = event => {
    this.loadCanvasFromImgFile(event.target.files[0], this.state.canvas, this.props.updateCanvas);
    this.setState({ fileLoaded: true })
  }

  handleTranslate() {
    const {canvas} = this.state;
    canvas.getObjects().forEach(obj => {})
  }

  loadCanvasFromImgFile(file, canvas, updateCanvas) {
    var reader = new FileReader();
    reader.onload = function (f) {
      var data = f.target.result;
      fabric.Image.fromURL(data, function (img) {
        // add background image
        canvas.setDimensions({ width: img.width, height: img.height });
        canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas));

        updateCanvas({ context: canvas.getContext('2d'), width: img.width, height: img.height });
      });
    };
    reader.onerror = function (e) {
      console.log(e);
    }
    // you have to declare the file loading
    reader.readAsDataURL(file);
  }

  render() {
    const children = React.Children.map(this.props.children, child => {
      return React.cloneElement(child, {
        canvas: this.state.canvas,
      })
    })
    const { width, height } = this.props
    return (
      <Fragment>
        <div style={{ height: '30px' }}>
          <input style={{ float: 'left' }} type='file' onChange={this.onChangeHandler} />
          {this.state.fileLoaded ? <button style={{ float: 'right' }}>Translate</button> : ''}
        </div>
        <div style={this.state.fileLoaded ? { display: 'block' } : { display: 'none' }}>
          <canvas ref={c => (this.c = c)} width={width} height={height} />
          {this.state.canvas && children}
        </div>
      </Fragment>
    )
  }
}

function mapState(state) {
  const { fields } = state;
  return { fields }
}

const actionCreators = {
  addField: fieldActions.addField,
  updateCanvas: canvasActions.updateCanvas,
  handleOnClickCanvas: fieldActions.changePosition
}

export default connect(mapState, actionCreators)(DesignCanvas)
