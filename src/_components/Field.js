import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import axios from 'axios';
import { authHeader } from '../_helpers';
import config from 'config';
import { fieldActions } from '../_actions';

class Field extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lang: this.props.lang ? this.props.lang : 'eng',
      result: this.props.result,
      isScaned: this.props.isScaned,
      id: this.props.id
    }
    this.handleScanField = this.handleScanField.bind(this)
    this.handleOnChangeLang = this.handleOnChangeLang.bind(this);
    this.handleDeleteField = this.handleDeleteField.bind(this);
  }

  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  handleScanField() {
    var items = this.props.fields.filter(item => item.id === this.state.id);
    let item = items[0];

    // create png of field
    var contextt = this.props.canvas.context.getImageData(item.position.x, item.position.y, item.position.width, item.position.height);
    var canvas = document.createElement("canvas");
    canvas.width = contextt.width;
    canvas.height = contextt.height;
    var context = canvas.getContext('2d');
    context.putImageData(contextt, 0, 0);
    var bg = canvas.toDataURL("image/png");
    const blob = this.b64toBlob(bg.substr(bg.indexOf(',') + 1), 'image/png');
    let img = new File([blob], 'runField.png', { type: "image/png" });

    var form_data = new FormData();
    form_data.append("file", img);
    form_data.append("lang", this.state.lang);
    const requestOptions = {
      method: 'post',
      headers: { ...authHeader() },
      data: form_data,
      url: `${config.apiUrl}/api/user/scan/single`
    };
    axios.request(requestOptions)
      .then(res => {
        this.setState({ result: res.data })
      });
  }

  handleOnChangeLang(event) { this.setState({ lang: event.target.value }) }

  handleDeleteField() {
    this.props.deleteField(this.props.id);
  }

  render() {
    return (
      <div style={{ backgroundColor: 'white', display: 'flow-root', margin: '10px' }}>
        <div style={{margin: '10px'}}>
          <p>single scan</p>
          <span>Language: </span><select onChange={this.handleOnChangeLang} value={this.state.lang}>
            <option>eng</option>
            <option>vie</option>
          </select><br></br>
          <span>Result: </span><p style={{ color: 'red' }}>{this.state.result}</p>
          <p><button onClick={this.handleScanField} style={{ float: 'right' }}>Scan</button>
            <button onClick={this.handleDeleteField} style={{ float: 'right' }}>Delete</button></p>
        </div>
      </div>
    )
  }
}

function mapState(state) {
  const { fields, canvas } = state;
  return { fields, canvas }
}

const actionCreators = {
  deleteField: fieldActions.deleteField
}

export default connect(mapState, actionCreators)(Field);