export function authHeader() {
    // return authorization header with jwt token
    let user = JSON.parse(localStorage.getItem('user'));

    if (user && user.data.access_token) {
        return { 'Authorization': 'Bearer ' + user.data.access_token };
    } else {
        return { 'Authorization': 'Basic cHJpbnRzdXBwb3J0Om15LXNlY3JldC1rZXk='};
    }
}