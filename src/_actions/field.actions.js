import { fieldConstants } from '../_constants/field.constants';

export const fieldActions = {
    addField,
    changePosition,
    deleteField
}

function addField(field) {
    return dispatch => {
        dispatch({
            type: fieldConstants.ADD_FIELD,
            field
        })
    }
}

function deleteField(id) {
    return dispatch => {
        dispatch({
            type: fieldConstants.DELETE_FIELD,
            id
        })
    }
}

function changePosition(canvasObjs) {
    return dispatch => {
        dispatch({
            type: fieldConstants.ON_CHANGE_POSITION,
            canvasObjs
        })
    }
}