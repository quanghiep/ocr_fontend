import { canvasConstants } from '../_constants';

export const canvasActions = {
    updateCanvas
}

function updateCanvas(canvas) {
    return dispatch => {
        dispatch({
            type: canvasConstants.UPDATE_CANVAS,
            canvas
        })
    }
}