import {canvasConstants} from '../_constants';

const initialState = {};
export function canvas(state = initialState, action) {
    switch(action.type) {
        case canvasConstants.UPDATE_CANVAS:
            return action.canvas;
        default: 
            return state;
    }
}