import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { registration } from './registration.reducer';
import { users } from './users.reducer';
import { alert } from './alert.reducer';
import { fields } from './field.reducer';
import { canvas } from './canvas.reducer'; 

const rootReducer = combineReducers({
  authentication,
  registration,
  users,
  alert,
  fields,
  canvas
});

export default rootReducer;