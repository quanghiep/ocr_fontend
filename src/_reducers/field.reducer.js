import {fieldConstants} from '../_constants/';

const initialState = [];
export function fields(state = initialState, action) {
    switch(action.type) {
        case fieldConstants.ADD_FIELD:
            return [ ...state, action.field];
        case fieldConstants.ON_CHANGE_POSITION:
            state.forEach(item => {
                if(action.canvasObjs.hasOwnProperty(item.id)){
                    item.position = action.canvasObjs[item.id];
                }
            });
            return state;
        case fieldConstants.DELETE_FIELD:
            return [...state.filter(item => item.id != action.id)];
        default: 
            return state;
    }
}