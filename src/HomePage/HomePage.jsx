import React from 'react';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import 'fabric-webpack';
import DesignCanvas from '../_components/DesignCanvas';
import Field from '../_components/Field';

class HomePage extends React.Component {
    componentDidMount() {
        this.props.getUsers();
    }

    handleDeleteUser(id) {
        return (e) => this.props.deleteUser(id);
    }

    render() {
        const { fields } = this.props;
        return (
            <div className="row">
                <div className='col-md-7'>
                    <DesignCanvas>
                    </DesignCanvas>
                </div>
                <div className='col-md-5'>
                    {fields.map((item, index) => {
                        return <Field lang={item.lang} result={item.result} isScaned={item.isScaned} id={item.id}></Field>
                    })}
                </div>
            </div>
        );
    }
}

function mapState(state) {
    const {fields} = state;
    return {fields}
}

const actionCreators = {
    getUsers: userActions.getAll,
    deleteUser: userActions.delete
}

const connectedHomePage = connect(mapState, actionCreators)(HomePage);
export { connectedHomePage as HomePage };